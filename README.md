# fake-dns

fake-dns provides DHCP/DNS services to an Emulation-as-a-Service network.
It is run by eaas-server in an additional emulation environment as part of the network.

EaaS's user interface allows to assign an IP address and
(fully qualified) domain name(s) (or a catch-all wildcard domain name) to each emulation environment in a network.
fake-dns converts these configured options to a dnsmasq configuration file and starts a dnsmasq server,
which then acts as DHCP/DNS server inside the emulated network.

Optionally, as configured by the user, fake-dns can route requests to unconfigured domain names
to a [slightly modified version of pywb](https://github.com/OpenSLX/pywb).
This allows clients (e.g., Web browsers, non-interactive Web clients) in the network environment to
transparently use the Web as archived by the [Wayback Machine](https://web.archive.org/) at a
user-configured date. (Currently, this only works for HTTP requests on port 80.)

A hybrid scenario, where, e.g., the Web service `api.example.com` is provided using an
emulation environment and the rest of the archived Web is served via pywb by the Wayback Machine,
is also possible.

## Pre-built Docker image

registry.gitlab.com/emulation-as-a-service/experiments/fake-dns
