# from alpine
# run apk add --no-cache dnsmasq jq
from registry.gitlab.com/emulation-as-a-service/experiments/pywb:proxy-ia
run apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y \
  dnsmasq jq busybox-static \
  && apt-get clean
workdir /eaas
copy run-dnsmasq generate-dnsmasq ./
cmd /eaas/run-dnsmasq
